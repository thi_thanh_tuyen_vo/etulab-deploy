package com.eservices.dao;


import java.sql.Connection;
import java.util.List;

import com.eservices.entity.Announcement;
import com.eservices.entity.Service;
import com.eservices.entity.User;
import com.eservices.entity.Category;
import com.eservices.entity.Message;

public interface Storage {

	public int addUser(User user) throws Exception;
	public User getUser(String email) throws Exception;
	public List<Announcement> getAnnouncements() throws Exception;
	public List<Announcement> getAnnouncementByKeyWord(String keyword) throws Exception;
	public List<Message> getLastMessageByConversation(String receiver) throws Exception;
	public List<Message> getDiscussionLines(String senderId, String receiverId) throws Exception;
	public int addMessage(String senderId, String receiverId, String newMessage, String newMessageTitle) throws Exception;
	public Service getService(int serviceId) throws Exception;
	//public int addAnnouncement(Announcement announcement);
	///public int addService(Service service);
	
	public Category getCategory(int categoryId) throws Exception;
	public int deleteUser();
	public int updateUserStatus();
	public int updateUserPhone();
	
}
