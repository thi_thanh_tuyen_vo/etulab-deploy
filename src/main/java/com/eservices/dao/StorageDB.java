package com.eservices.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.eservices.entity.Announcement;
import com.eservices.entity.Category;
import com.eservices.entity.Message;
import com.eservices.entity.Service;
import com.eservices.entity.User;

public class StorageDB implements Storage {

	private Connection connection = null;
	private Statement statement = null;
	
	public StorageDB(String url, String username, String password) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection(url, username, password);
			this.statement = connection.createStatement();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int addUser(User user) throws Exception{

		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users(user_id, user_firstname, user_lastname, user_email, user_password, user_status, user_profil, user_phone, user_photo) VALUES (?,?,?,?,?,?,?,?,?);");
		preparedStatement.setInt(1, 0);
		preparedStatement.setString(2, user.getFirstname());
        preparedStatement.setString(3, user.getLastname());
        preparedStatement.setString(4, user.getEmail());
        preparedStatement.setString(5, user.getPassword());
        preparedStatement.setInt(6, user.getStatus());
        preparedStatement.setString(7, user.getProfile());
        preparedStatement.setString(8, user.getPhone());
        preparedStatement.setString(9, user.getPhoto());
        
        int result = preparedStatement.executeUpdate();
        return result;
		//this.statement.executeUpdate("INSERT INTO users VALUES (3, 'Philemon', 'St Jean', 'philemon3006@yahoo.fr', '12345', '2', 'etudiant L3 info avec experience en logistique', '0627178055', 'photo.jpg');");
	}

	@Override
	public int deleteUser() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateUserStatus() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateUserPhone() {
		// TODO Auto-generated method stub
		return 0;
	}

	public User getUser(String loginName) throws Exception {
		User user = new User();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from users "
				+ "WHERE user_id = ?;");
		preparedStatement.setString(1, loginName);
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			
			user.setId(result.getInt("user_id"));
			user.setFirstname(result.getString("user_firstname"));
			user.setLastname(result.getString("user_lastname"));
			user.setStatus(result.getInt("user_status"));
			user.setProfile(result.getString("user_profil"));
			user.setEmail(result.getString("user_email"));
			user.setPassword(result.getString("user_password"));
			user.setPhone(result.getString("user_phone"));
			user.setPhoto(result.getString("user_photo"));
		}
		return user;
	}

	@Override
	public List<Announcement> getAnnouncements() throws Exception{
		List<Announcement> announcements = new ArrayList();
		
		// retourner toutes les annonces qui sont actif
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from announcement"); 
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			Announcement announcementsItem = new Announcement();
			announcementsItem.setAnnouncementId(result.getInt("announcement_id"));
			announcementsItem.setContractor(getUser(result.getString("contractor_id"))); // objet User a recuperer avec getUser
			announcementsItem.setService(getService(result.getInt("service_id")));; // objet Service a recuperer avec getService
			announcementsItem.setAnnouncement_date(result.getString("announcement_date_time"));
			announcementsItem.setService_price(result.getDouble("service_price"));
			announcements.add(announcementsItem);
		}
		return announcements;
	}
	
	public List<Announcement> getAnnouncementByKeyWord(String keyword) throws Exception {
		List<Announcement> announcements = new ArrayList();
		
		// retourner toutes les annonces qui sont actif
				PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM "
						+ "announcement WHERE service_id IN(SELECT service_id "
						+ "from service where service_description LIKE ? OR service_title LIKE ?)"); 
				preparedStatement.setString(1, keyword);
				preparedStatement.setString(2, keyword);
				ResultSet result = preparedStatement.executeQuery();
				
		while(result.next()) {
			Announcement announcementsItem = new Announcement();
			announcementsItem.setAnnouncementId(result.getInt("announcement_id"));
			announcementsItem.setContractor(getUser(result.getString("contractor_id"))); // objet User a recuperer avec getUser
			announcementsItem.setService(getService(result.getInt("service_id")));; // objet Service a recuperer avec getService
			announcementsItem.setAnnouncement_date(result.getString("announcement_date_time"));
			announcementsItem.setService_price(result.getDouble("service_price"));
			announcements.add(announcementsItem);
		}
		return announcements;
	}
	
	@Override
	public Service getService(int serviceId) throws Exception {
		
		Service service = new Service();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from service "
				+ "WHERE service_id = ?;");
		preparedStatement.setInt(1, serviceId);
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			
			service.setServiceId(result.getInt("service_id"));
			service.setService_title(result.getString("service_title"));
			service.setService_description(result.getString("service_description"));
			service.setService_duration(result.getString("service_duration"));
			service.setStudent(getUser(result.getString("user_id")));
			service.setService_category(getCategory(result.getInt("service_category")));
			
		}
		return service;
	}

	@Override
	public Category getCategory(int categoryId) throws Exception {
		Category category = new Category();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from categories WHERE category_id = ?;");
		preparedStatement.setInt(1, categoryId);
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			
			category.setCategoryId(result.getInt("category_id"));
			category.setCategoryName(result.getString("category_name"));
		}
		return category;
	}

	public List<Message> getLastMessageByConversation(String receiver) throws Exception{
		List<Message> lastMessages = new ArrayList();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM message "
				+ "WHERE receiver_id = ? AND message_date_time IN ( SELECT MAX(message_date_time) "
				+ "FROM message GROUP BY sender_id) ORDER BY message_date_time DESC ");
		preparedStatement.setString(1, receiver);
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			Message msg = new Message();
			msg.setMessageId(result.getInt("message_id"));
			msg.setMessageDateTime(result.getString("message_date_time"));
			msg.setMessageContent(result.getString("message_content"));
			msg.setMessageTitle(result.getString("message_title"));
			msg.setReceiver(getUser(result.getString("receiver_id")));
			msg.setSender(getUser(result.getString("sender_id")));	
			lastMessages.add(msg);
		}
		return lastMessages;
	}

	public int getTotalUnreadMessage(String receiver) throws Exception{
		// on ajoute un champs read (0/1) dans la table message
		return 0;
	}

	public List<Message> getDiscussionLines(String senderId, String receiverId) throws Exception{
		List<Message> messagesDiscussion = new ArrayList();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM message "
				+ "WHERE receiver_id = ? AND sender_id = ? OR sender_id = ? AND receiver_id = ?"
				+ "ORDER BY message_date_time ASC");

		preparedStatement.setString(1, receiverId);
		preparedStatement.setString(2, senderId);
		preparedStatement.setString(3, receiverId);
		preparedStatement.setString(4, senderId);
		
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			Message msg = new Message();
			msg.setMessageId(result.getInt("message_id"));
			msg.setMessageDateTime(result.getString("message_date_time"));
			msg.setMessageContent(result.getString("message_content"));
			msg.setMessageTitle(result.getString("message_title"));
			msg.setReceiver(getUser(result.getString("receiver_id")));
			msg.setSender(getUser(result.getString("sender_id")));	
			messagesDiscussion.add(msg);
		}
		return messagesDiscussion;
	}

	public int addMessage(String senderId, String receiverId, String newMessage, String newMessageTitle) throws Exception{
	
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO message(sender_id, "
				+ "receiver_id, message_date_time, message_content, message_title) VALUES(?,?,NOW(),?,?)");
		preparedStatement.setInt(1, Integer.valueOf(senderId));
		preparedStatement.setInt(2, Integer.valueOf(receiverId));
		preparedStatement.setString(3, newMessage);
		preparedStatement.setString(4, newMessageTitle);
		
		int result = preparedStatement.executeUpdate();
		return result;
	}

	
	
	

}
