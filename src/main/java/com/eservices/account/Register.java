package com.eservices.account;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.StorageDB;
import com.eservices.entity.User;


@WebServlet("/register")
public class Register extends HttpServlet {
	
	private static final int REGISTER_SUCCESSFUL = 1;
	private static final int REGISTER_FAILED = 0;
	private static final long serialVersionUID = 1L;
       
   
    public Register() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// doGet(request, response);
		int result = 0;
		try {
			
			StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
			
			User user = new User();
			user.setLastname(request.getParameter("lastname"));
			user.setFirstname(request.getParameter("firstname"));
			user.setEmail(request.getParameter("email"));
			user.setPassword(request.getParameter("password"));
			user.setStatus(Integer.valueOf(request.getParameter("status")));
			user.setProfile(request.getParameter("profile"));
			user.setPhone(request.getParameter("phone"));
			user.setPhoto(request.getParameter("photo"));
			
			result = mysql_server.addUser(user);
			/* penser a ajouter des balises de controle pour valider l'information
			 * venant du formulaire client  pour s'assurer que les donnees sont aux normes
			 * @author: Philemon, date: 2 mai 2022
			 */
			
			request.setAttribute("result", result);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		// this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		
		/* si l'enregistrement de l'utilisateur reussit: 
		*		- on cree une nouvelle variable session dans laquelle on garde certaines donnees user(nom, prenom, id, etc...)  
		*		- on redirige vers la page home
		* @author: Philemon, date: 2 mai 2022
		*/
		if(result == REGISTER_SUCCESSFUL) {
			response.sendRedirect(request.getContextPath() + "/login");
			return;
		}else {
			
		}
		
	}

}
