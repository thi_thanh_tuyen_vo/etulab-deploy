package com.eservices.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Announcement;

/**
 * Servlet implementation class Search
 */
@WebServlet("/search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Search() {
        super();
       
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String search_topic = request.getParameter("q");
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		List<Announcement> announcement_list = null;
		try {
			// recupere toutes les annonces de la BDD
			announcement_list = mysql_server.getAnnouncementByKeyWord("%"+search_topic+"%"); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		// transmet une ArrayList d'annonces a la page d'accueil
				request.setAttribute("announcement_list", announcement_list);
				request.setAttribute("search_topic", search_topic);
		doGet(request, response);
	}

}
