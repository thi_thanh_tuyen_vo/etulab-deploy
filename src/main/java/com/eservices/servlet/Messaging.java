package com.eservices.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Announcement;
import com.eservices.entity.Message;

/**
 * Servlet implementation class Message
 */
@WebServlet("/message")
public class Messaging extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public Messaging() {
        super();
      
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String senderId = request.getParameter("s");
		String receiverId = request.getParameter("r");
		
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		List<Message> messageDiscussion = null;
		int totalUnreadMessage = 0;
		
		try {
			messageDiscussion = mysqlServer.getDiscussionLines(senderId, receiverId); 
			//totalUnreadMessage = mysqlServer.getTotalUnreadMessage("");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// transmet une ArrayList d'annonces a la page d'accueil
		request.setAttribute("messages_discussion", messageDiscussion);
		request.setAttribute("receiverId", receiverId);
		request.setAttribute("senderId", senderId);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/message.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String senderId = request.getParameter("s");
		String receiverId = request.getParameter("r");
		String newMessage = request.getParameter("reply");
		String newMessageTitle = request.getParameter("reply_title");
		Message message = new Message();
		
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		List<Message> messageDiscussion = null;
		int totalUnreadMessage = 0;
		
		try {
			if(!newMessage.isBlank()) {
				mysqlServer.addMessage(senderId, receiverId, newMessage, newMessageTitle); 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		doGet(request, response);
	}

}
