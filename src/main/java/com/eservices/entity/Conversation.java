package com.eservices.entity;

import java.util.List;

public class Conversation {
	private List<Message> conversationMessages;
	private int conversationUnreadMessages;
	
	public List<Message> getConversationMessages() {
		return conversationMessages;
	}
	public void setConversationMessages(List<Message> conversationMessages) {
		this.conversationMessages = conversationMessages;
	}
	public int getConversationUnreadMessages() {
		return conversationUnreadMessages;
	}
	public void setConversationUnreadMessages(int conversationUnreadMessages) {
		this.conversationUnreadMessages = conversationUnreadMessages;
	}
}
