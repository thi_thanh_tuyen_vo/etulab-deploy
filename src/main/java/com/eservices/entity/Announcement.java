package com.eservices.entity;

public class Announcement {
	
	private int announcementId;
	private Service service;
	private User contractor;
	private String announcement_date;
	private double service_price;
	
	public int getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(int announcementId) {
		this.announcementId = announcementId;
	}
	
	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
	public User getContractor() {
		return contractor;
	}
	public void setContractor(User contractor) {
		this.contractor = contractor;
	}
	public String getAnnouncement_date() {
		return announcement_date;
	}
	public void setAnnouncement_date(String announcement_date) {
		this.announcement_date = announcement_date;
	}
	public double getService_price() {
		return service_price;
	}
	public void setService_price(double service_price) {
		this.service_price = service_price;
	}
	
}
