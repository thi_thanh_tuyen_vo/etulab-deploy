package com.eservices.entity;

public class Service {
	
	private int serviceId;
	private User student;
	private String service_title;
	private String service_description;
	private String service_duration;
	private Category service_category;
	
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	public User getStudent() {
		return student;
	}
	public void setStudent(User student) {
		this.student = student;
	}
	public String getService_title() {
		return service_title;
	}
	public void setService_title(String service_title) {
		this.service_title = service_title;
	}
	public String getService_description() {
		return service_description;
	}
	public void setService_description(String service_description) {
		this.service_description = service_description;
	}
	public String getService_duration() {
		return service_duration;
	}
	public void setService_duration(String service_duration) {
		this.service_duration = service_duration;
	}
	public Category getService_category() {
		return service_category;
	}
	public void setService_category(Category service_category) {
		this.service_category = service_category;
	}
	
	
}

