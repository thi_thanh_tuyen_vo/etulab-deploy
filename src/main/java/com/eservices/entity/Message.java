package com.eservices.entity;

public class Message {
	private int messageId;
	private User sender;
	private User receiver;
	private String messageDateTime;
	private String messageContent;
	private String messageTitle;
	
	public String getMessageTitle() {
		return messageTitle;
	}
	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int message_id) {
		this.messageId = message_id;
	}
	public User getSender() {
		return sender;
	}
	public void setSender(User sender) {
		this.sender = sender;
	}
	public User getReceiver() {
		return receiver;
	}
	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}
	public String getMessageDateTime() {
		return messageDateTime;
	}
	public void setMessageDateTime(String message_date_time) {
		this.messageDateTime = message_date_time;
	}
	public String getMessageContent() {
		return messageContent;
	}
	public void setMessageContent(String message_content) {
		this.messageContent = message_content;
	}
	
	
}
