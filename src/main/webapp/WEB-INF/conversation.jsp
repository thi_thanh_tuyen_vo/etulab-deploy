<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Messages</title>
</head>
<body>
	<!-- Page header -->
	<%@ include file="header.jsp" %>
	<%@ include file="menu.jsp" %>
	
	
	<div class="w3-container">
	<h3>Conversations</h3>
		<div class="w3-center" style="margin:auto;">
			<table class="w3-table w3-col m6">
				<c:forEach items="${ messages_list }" var="message" varStatus="status">
					<tr class="w3-hover-purple">
						<td>
							&#x2709;
						</td>
						<td>
							<c:out value="${ message.sender.firstname}" />
							<c:out value="${ message.sender.lastname}" />
						</td>
						<td>
							<a href="/EtuJob/message?s=${ message.sender.id}&r=${ message.receiver.id}" style="text-decoration:none;">
								<c:out value="${ message.messageTitle}" />
							</a>
						</td>
						<td>
							<c:out value="${ message.messageDateTime }" />
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<!-- Page footer -->
	<%@ include file="footer.jsp" %>
</body>
</html>