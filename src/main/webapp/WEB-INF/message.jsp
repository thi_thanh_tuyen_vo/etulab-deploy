<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Messagerie</title>
</head>
<body>
	<!-- Header -->
<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>
	
	<div class="w3-container">
		<a href="/EtuJob/conversations" style="text-decoration:none;"><h2>&#x25c1;</h2></a>
		<h3>Messagerie</h3>
	

		<c:forEach items="${ messages_discussion }" var="message" varStatus="status">
			<div class="w3-row">
				<h5><c:out value="${ message.sender.firstname}" /> / <c:out value="${ message.messageTitle}" /></h5>
				<p class="w3-col m4"><c:out value="${ message.messageContent}" /></p>
				<p><c:out value="${ message.messageDateTime}" /></p>
			</div>
		</c:forEach>
		
		<div class="w3-yellow">
			<form class="w3-col m7 w3-margin" action="message" method="POST">
				<div class="w3-col m6">
					<h5 class="w3-row">new message</h5>
					<input type="hidden" value="<c:out value="${ senderId}"/>" name="s"/>
					<input type="hidden" value="<c:out value="${ receiverId}"/>" name="r"/>
					<input class="w3-margin" type="text" name="reply_title" placeholder="Titre de mon message"/>
					<textarea class="w3-input w3-light-gray w3-border-0 w3-margin" rows="4" cols="20" id="reply" name="reply"></textarea>
					<input class="w3-button w3-purple" style="margin-left:250px; border-color:purple" type="submit" value="Send &#x27a4;"/>
				</div>		
			</form>
		</div>
	
	</div>
	
	<!-- footer -->
	<%@ include file="footer.jsp" %>
</body>
</html>