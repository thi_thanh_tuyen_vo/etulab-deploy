<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<div class="w3-center" style="margin:auto; margin-top:20px;margin-bottom:20px;">
		<form action="search" method="POST">
			<input type="text" size="50" placeholder="Search" value="<c:out value="${search_topic}"/>" name="q" style="border-radius:10px"/>
			<input class="w3-btn w3-round-xxlarge w3-hover-purple" type="submit" value="&#128269;"/>
		</form>
	</div>
</head>
<body>

</body>
</html>

