
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<meta charset="UTF-8">
<title>Welcome to Login page</title>
</head>

<body>
	<%@ include file="header.jsp" %>
	<div class="w3-center" style="margin:auto;">
		
		<form class="w3-container" method="POST" action="login">
		<div style="margin-bottom:6px;">
			<h3>Login page</h3>
		</div>
			<input style="margin: 4px;" type="text" name="login" placeholder="login@etujob.fr"/>
			<br><input style="margin: 4px;" type="password" name="password"/>
			<br><input class="w3-button w3-hover-purple" style="margin: 4px; background:#bdcebe" type="submit" value="Login"/>
			<br>Don't have an account ? <a class="w3-text-red" href="/EtuJob/register">Register</a>
			
		</form>
	</div>
</body>
</html>