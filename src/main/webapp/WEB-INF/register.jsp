<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<meta charset="UTF-8">
<title>Registration page !</title>
</head>

<body>
<%@ include file="header.jsp" %>
<div class="w3-container">
	<div class="w3-col m4 w3-purple">
		<div class="w3-container">
			<h3>Registration Form</h3>
		</div>
		<form class="w3-container" method="POST" action="register">
			<label>Nom</label>
			<input class="w3-input w3-border-0" type="text" id="lastname" name="lastname" placeholder="lastname"/>
				
			<label>Prenom</label>
			<input class="w3-input w3-border-0" type="text" id="firstname" name="firstname" placeholder="firstname"/>
				
			<label for="email" >Email</label>
			<input class="w3-input w3-border-0" type="text" id="email" name="email" placeholder="madeleine@email.fr"/>
				
			<label for="password">Password</label>
			<input class="w3-input w3-border-0"  type="password" id="password" name="password"/>
			
			<input class="w3-input w3-border-0" type="hidden" id="status" value ="2" name="status"/>
			
			<label for="profile">Profile</label>
			<textarea class="w3-input w3-border-0" rows="10" id="profile" name="profile"> Please describe your profile here 
			your background 
			your skills
			
			past experience
			your promises
			</textarea>
			
			<label for="phone">telephone</label>
			<input class="w3-input w3-border-0" type="text" id="phone" name="phone"  placeholder="0800 55 55 55"/>
			
			<label for="photo">Your photo here</label>
			<input class="w3-input w3-border-0"  type="file" id="photo" name="photo"/>
					
			<button class="w3-btn w3-blue-grey">Register</button>
		</form>
	</div>
	<div class="w3-col m8">
		<img style="opacity: 0.5" src="${pageContext.request.contextPath}/img/calanques.jpg"/ width="" height="">
	</div>
	
</div>
</body>
</html>